import { writable, readable, derived, get } from 'svelte/store';

/**
 * Can use lodash for working with arrays, objects and collections
 * @url https://lodash.com/
*/
import _ from 'lodash';

/**
 * Can use axios for ajax requests
 * @url https://github.com/axios/axios
*/
import axios from 'axios';

export const HistoryStore = function () {
    let data = [];

    const { subscribe, set, update, get } = writable(data);

	return {
        set,
		subscribe,
		language: (lng) => { /** Filter Language */},
	};
}();

export const AwardStore = function () {
    let data = [];

    const { subscribe, set, update, get } = writable(data);

	return {
        set,
		subscribe,
		language: (lng) => { /** Filter Language */},
	};
}();

export const AllStore = function () {
    let data = [];

    const { subscribe, set, update, get } = writable(data);

    axios.get('/json/history.json')
        .then((response) => {

            let items = _.map(response.data.timeline.date, (item,index) => {
                item.id = index+1;
                return item;
            });

            set(items);

            HistoryStore.set(_.filter(items,{ 'type': 'history',}));
            AwardStore.set(_.filter(items,{ 'type': 'award',}));
        });

	return {
        set,
		subscribe,
		language: (lng) => { /** Filter Language */},
	};
}();
