import { writable, readable, derived } from 'svelte/store';

/**
 * Can use lodash for working with arrays, objects and collections
 * @url https://lodash.com/
*/
// import _ from 'lodash';

/**
 * Can use axios for ajax requests
 * @url https://github.com/axios/axios
*/
// import axios from 'axios';


/**
 * Translatable Static Strings example
 * $language.set('ru) will also change ss store data
 */
export const ss = function () {
	let data = []

	const { subscribe, set, update } = writable(data);

	return {
		subscribe,
		language: (lng) => { /** Filter Language */},
	};
}();


export const language = function () {
	let initialLang = 'lv';

	const { subscribe, set, update } = writable(initialLang);
	return {
		subscribe,
		set,
		update
	};
}();

language.subscribe(value => {
	ss.language(value);
})
