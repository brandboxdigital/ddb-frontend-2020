export const colorfulColorScheme = [
    '#62F5AC',
    '#FFBBA1',
    '#FCF115',
    '#A7ACFE'
];

export const neutralColorScheme = [
    '#C1B790',
    '#FFBBA1',
    '#C9D4C7',
    '#A4BBA5'
];

export const baseColorScheme = [
    '#1A37D4',
    '#FFCB00',
    '#EE1D15',
];
