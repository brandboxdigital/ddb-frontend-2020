/**
 * Returns if browser supports object-fit
 * @returns {boolean}
 */
export function hasObjectFit () {
	return ( 'objectFit' in document.documentElement.style !== false );
}

/**
 * Returns if browser supports touch events
 * @returns {boolean}
 */
export function hasTouchEvents () {
	return ( 'ontouchstart' in document.documentElement !== false );
}