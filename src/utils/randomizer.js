export function getRandom (min, max) {
    return Math.random() * (max - min) + min;
}

export function getRandomInt (min, max) {
    const number = Math.random() * (max - min) + min;
    return Math.round(number);
}

export function randomDate(start, end) {
    return new Date(start.getTime() + Math.random() * (end.getTime() - start.getTime()));
}
