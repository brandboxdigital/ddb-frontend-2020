import PropTypes from 'prop-types';
import VideoEmbed from '../components/video-embed';

export const quoteProps = {
	text: PropTypes.string.isRequired,
	textColor: PropTypes.string.isRequired,
	bgColor: PropTypes.string.isRequired
};

export const titleBodyProps = {
	title: PropTypes.string,
	body: PropTypes.string,
	keywords: PropTypes.arrayOf( PropTypes.string )
};

export const imageProps = {
	path: PropTypes.string.isRequired
};

export const videoProps = {
	path: PropTypes.string.isRequired
};

export const videoEmbedProps = {
	// videoEmbed: VideoEmbed.propTypes.videoEmbed // Causes problems with the static build?
};

export const imageVideoProps = {
	image: PropTypes.shape( imageProps ).isRequired,
	video: PropTypes.shape( videoProps ),
	videoEmbed: PropTypes.shape( videoEmbedProps )
};

export const stickyTextImagesProps = {
	body: PropTypes.string.isRequired,
	imageVideos: PropTypes.arrayOf( PropTypes.element ).isRequired,
};

export const stickyImageBodyProps = {
	imageVideo: PropTypes.element.isRequired,
	body: PropTypes.string.isRequired,
};

export const twoImageVideoProps = {
	leftImageVideo: PropTypes.element.isRequired,
	rightImageVideo: PropTypes.element.isRequired
};

export const socialprops = {
	contact: PropTypes.string.isRequired,
	jobs: PropTypes.string.isRequired,
	accounts: PropTypes.string.isRequired
};

export const seoProps = {
	image: imageProps.isRequired,
	title: PropTypes.string.isRequired,
	description: PropTypes.string.isRequired
};

export const clientProps = {
	name: PropTypes.string.isRequired
};

export const tagProps = {
	name: PropTypes.string.isRequired,
};

export const caseProps = {
	name: PropTypes.string.isRequired,
	slug: PropTypes.string.isRequired,
	description: PropTypes.string,
	image: PropTypes.shape( imageProps ).isRequired,
	client: PropTypes.shape( clientProps ).isRequired,
	tags: PropTypes.arrayOf( PropTypes.shape( tagProps ) ).isRequired,
	modules: PropTypes.array.isRequired
};

export const newsProps = {
	seo: PropTypes.shape( seoProps ).isRequired,
	modules: PropTypes.array.isRequired
};

export const startPageProps = {
	seo: PropTypes.shape( seoProps ).isRequired,
};

export const workPageProps = {
	seo: PropTypes.shape( seoProps ).isRequired,
};