export default function ( array, rawIndex, offset ) {
	if( offset > 0 ) {
		return ( rawIndex + offset ) % array.length;
	} else if ( offset < 0 ) {
		const index = rawIndex + offset;
		return index < 0 ? array.length + index : index;
	} else {
		return rawIndex;
	}
}