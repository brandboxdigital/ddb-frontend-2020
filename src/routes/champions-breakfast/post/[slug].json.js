const posts = {

    'rihards-ginters-dots-devejam-atdodas': {
        slug: 'rihards-ginters-dots-devejam-atdodas',
        vimeoId: '378303948',
        title: 'Champion Breakfast #38',
        subtitle: 'Rihards Ginters - Dots devējam atdodas!',
        description: '',
        year: '2019',
    },
    'maija-rozenfelde-memo-pardeveju-sarunas': {
        slug: 'maija-rozenfelde-memo-pardeveju-sarunas',
        vimeoId: '378310951',
        title: 'Champion Breakfast #38',
        subtitle: 'Maija Rozenfelde - Mēmo pārdevēju sarunas',
        description: '',
        year: '2019',
    },
    'raimonds-tomsons-ka-sasniegt-pasaules-slavu-latvija-nepieredzejusa-nozare': {
        slug: 'raimonds-tomsons-ka-sasniegt-pasaules-slavu-latvija-nepieredzejusa-nozare',
        vimeoId: '378314952',
        title: 'Champion Breakfast #38',
        subtitle: 'Raimonds Tomsons - Kā sasniegt pasaules slavu Latvijā nepieredzējušā nozarē?',
        description: '',
        year: '2019',
    },
    'elina-branta-ka-pardot-latviju-biznesa-iespeju-zemi': {
        slug: 'elina-branta-ka-pardot-latviju-biznesa-iespeju-zemi',
        vimeoId: '361279032',
        title: 'Champion Breakfast #37',
        subtitle: 'Elīna Branta - Kā pārdot Latviju - biznesa iespēju zemi?',
        description: '',
        year: '2019',
    },
    'annija-speka-maksligais-intelekts-inteligentam-biznesam': {
        slug: 'annija-speka-maksligais-intelekts-inteligentam-biznesam',
        vimeoId: '361279220',
        title: 'Champion Breakfast #37',
        subtitle: 'Annija Spēka - Mākslīgais intelekts inteliģentam biznesam',
        description: '',
        year: '2019',
    },
    'aigars-nords-cels-uz-zeltu': {
        slug: 'aigars-nords-cels-uz-zeltu',
        vimeoId: '361279533',
        title: 'Champion Breakfast #37',
        subtitle: 'Aigars Nords - Ceļš uz zeltu',
        description: '',
        year: '2019',
    },
    'conversation-festival-lampa-anna-celmina-vai-zimoli-drikst-glabt-pasauli': {
        slug:'conversation-festival-lampa-anna-celmina-vai-zimoli-drikst-glabt-pasauli',
        vimeoId: '349415183',
        title: 'Champion Breakfast #36',
        subtitle: 'Conversation festival LAMPA Anna Celmiņa - “Vai zīmoli drīkst glābt pasauli?”',
        description: '',
        year: '2019',
    },
};

export const get = (req, res) => {

    function takevalues(item) {
        return {
            slug: item.slug,
            cover: item.cover,
            title: item.title,
            client: item.client,
            vimeoId: item.vimeoId,
        };
    }

    function next(item) {
        let keys = Object.keys(posts);
        let index = keys.indexOf(item) + 1;

        if (index >= keys.length) {
            index = 0;
        }

        return takevalues(posts[keys[index]]);
    }

    function prev(item) {
        let keys = Object.keys(posts);
        let index = keys.indexOf(item) - 1;

        if (index < 0) {
            index = keys.length - 1;
        }

        return takevalues(posts[keys[index]])
    }

    const { slug } = req.params
    let returnValue = null;

    console.log('New slug:', slug);

    if (slug == 'all') {
        returnValue = posts;
    } else {
        try {
            returnValue = posts[slug];
            returnValue.next = next(slug);
            returnValue.prev = prev(slug);
        } catch (e) {
            returnValue = '';
        }
    }

    res.writeHead(200, { 'Content-Type': 'application/json' })
    res.end( JSON.stringify( returnValue ) )
}
