const cases = {

    'skpc-home-soap-2meters':{
        slug: 'skpc-home-soap-2meters',
        titleBlock: { type: 'vimeo', col: 12, videoId: '447816128', coverVideo: '/assets/img/cases/skpc/SPKC-home-soap-2metres.mp4' },
        cover: '/assets/img/cases/skpc/DDB_SPKC_1100x1200px_02.jpg',
        video: "",
        video_loop: "",
        title: 'Home, Soap, 2 Meters',
        client: 'SPKC',
        cat: 'identity',
        excerpt: 'A COVID-related campaign more contagious than the virus itself? Wasn\'t easy, but we\'re pretty sure we made it work.',
        description: "As if containing the pandemic wasn't enough of a challenge, our Centre for Disease Prevention and Control also had a communication job to do when COVID hit. They needed to respond instantly and put out a clear and universal message. So we helped them do it. If there's one thing that will remain in the Latvian folklore from this time, it's our rhyme.",
        blocks: [
            {type: 'image', col: 6, data: '/assets/img/cases/skpc/SPKC_KV_pelēks.png' },
            {type: 'image', col: 6, data: '/assets/img/cases/skpc/DDB_SPKC_1100x1200px_02.jpg' },
            {type: 'image', col: 12, data: '/assets/img/cases/skpc/SPKCbanneri-7.jpg' },
            {type: 'image', col: 12, data: '/assets/img/cases/skpc/SPKCbanneri-17.jpg' },
        ],
    },

    'goodlife':{
        slug:'goodlife',
        titleBlock: {type: 'vimeo', col: 12, videoId: '294306712', coverVideo: '/assets/img/cases/rimi/loop.webm'},
        cover:'/assets/img/cases/rimi.jpg',
        video:"",
        video_loop:"",
        title:'Goodlife',
        client:'Rimi',
        cat: 'identity',
        excerpt: 'Finally someone has deciphered all those ingredient lists and clearly labelled every healthy choice on supermarket shelves. To let shoppers know, we wrote probably the first song in the world composed of ingredient lists.',
        description:"Making healthy choices is nowhere as easy as we would like. But finally there's a supermarket that has done all the heavy lifting to decipher all those ingredient lists, and clearly labelled every healthy choice on their shelves. How do we send a loud and clear message about this to the shoppers? With a song composed of the most enigmatic ingredients – highlighting how crazy it is, and offering a shortcut to better choices.",
        blocks:[
            {type: 'vimeo', col: 12, videoId: '357508950', coverVideo: '/assets/img/cases/rimi/labakai-dzivei-case-web.webm' },
            {type: 'image', col: 12, data: '/assets/img/cases/rimi/DSC02660.jpg' },
            {type: 'image', col: 6, data: '/assets/img/cases/rimi/DSC02639.jpg' },
            {type: 'image', col: 6, data: '/assets/img/cases/rimi/DSC02634.jpg' },
            // {type: 'image', col: 12, data: '/assets/img/cases/rimi/D75_4743LV.jpg' },
        ],
    },

    'pupkultura':{
        slug:'pupkultura',
        titleBlock: {type: 'vimeo', col: 12, videoId: '380510576', coverVideo:"/assets/img/cases/pupkultura/loop.webm" },
        cover:'/assets/img/cases/pupkultura.jpg',
        title:"#Pupkultūra",
        client:'The Pink Train Foundation',
        cat: 'identity',
        excerpt: "How come pop culture is packed with boobs, but actual breast health is still a taboo, killing women? Looking to change this for the better, we hijacked the love for boobs to raise awareness and funds to help battle breast cancer.",
        description:"",
        blocks:[
            {
                type: 'sticky',
                title:"#PUPKULTŪRA (#BoobCulture)",
                description:"It started from a modest request to redesign donation boxes for a breast cancer charity. We thought that was far from enough, so with our help it grew into a whole movement, shamelessly hijacking the popularity of boobs in pop culture to use it for a nobler goal. Our campaign was provocative, loud and efficient. Even if we didn't exactly have a budget, our guerrilla tactics and our partners with hearts in the right place helped reach all charity goals, and the movement is still alive and kicking.",
                blocks: [
                    {type: 'image', col: 12, data: '/assets/img/cases/pupkultura/1.jpg' },
                    {type: 'image', col: 12, data: '/assets/img/cases/pupkultura/2.jpg' },
                    {type: 'image', col: 12, data: '/assets/img/cases/pupkultura/3.jpg' },
                    {type: 'image', col: 12, data: '/assets/img/cases/pupkultura/4.jpg' },
                ],
            },
            {type: 'image', col: 6, data: '/assets/img/cases/pupkultura/5.png' },
            {type: 'image', col: 6, data: '/assets/img/cases/pupkultura/6.png' },
            {type: 'image', col: 6, data: '/assets/img/cases/pupkultura/8.jpg' },
            {type: 'image', col: 6, data: '/assets/img/cases/pupkultura/9.jpg' },
            {type: 'image', col: 12, data: '/assets/img/cases/pupkultura/7.jpg' },
        ],
    },

    'its-all-in-our-hands':{
        slug:'its-all-in-our-hands',
        titleBlock: {type: 'vimeo', col: 12, videoId: '307074248', coverVideo: '/assets/img/cases/lmt/loop.webm'},

        cover:'/assets/img/cases/lmt.jpg',
        title:"It's All In Our Hands",
        client:'LMT',
        cat: 'identity',
        excerpt: 'As any mobile operator, LMT wants to put a new phone in your hands. But they also sincerely want to change the society for the better. So we reminded people that not just our phones, but our destiny is also in our hands.',
        description:"Every mobile operator wants people to change their phones to new ones at Christmas. But LMT likes to take on a bigger responsibility and create change on a different level, changing mindsets too. Latvians are famously pessimistic – and that's a challenge we happily accepted. To inspire confidence that our fate is in our own hands, we quite literally put a lot of hands to work, producing a shadow play. Set to an encouraging song, of course.",
        blocks:[
            // {type: 'vimeo', col: 12, videoId: '307074248' },
            {type: 'vimeo', col: 12, videoId: '357502308', coverVideo: '/assets/img/cases/lmt/laimes-lacis-case-web.webm' },

            {type: 'image', col: 12, data: '/assets/img/cases/lmt/1.jpg' },
            {type: 'image', col: 12, data: '/assets/img/cases/lmt/2.jpg' },
            {type: 'image', col: 6,  data: '/assets/img/cases/lmt/3.jpg' },
            {type: 'image', col: 6,  data: '/assets/img/cases/lmt/4.jpg' },
            {type: 'image', col: 12, data: '/assets/img/cases/lmt/5.jpg' },
            {type: 'image', col: 12, data: '/assets/img/cases/lmt/6.jpg' },
        ],
    },

    'nationwide-sortathlon':{
        slug:'nationwide-sortathlon',
        cover:'/assets/img/cases/lzp.png',
        titleBlock: {type: 'vimeo', col: 12, videoId: '357361647', coverVideo: '/assets/img/cases/lzp/loop.webm'},
        title:"Nationwide Sortathlon",
        client:'Latvijas Zaļais Punkts',
        cat: 'identity',
        excerpt: 'We want recycling to be fun. So to talk about it we had some fun ourselves – turning the whole thing into a sport and getting our hands dirty recycling puns, song lyrics, famous motivation quotes and a shedload of other things.',
        description:"Way too many Latvians think recycling is hard, so why should they bother. Then again, when it's about sport, people bother exactly because it's hard. That's why we turned recycling into a sports discipline for The Latvian Green Dot. And to make it fun, we assigned a trash-talking coach to lead the movement and put our punning skills to work. Our coach was even invited to appear at half-time of Latvia – Portugal football game and to do a stand-up on the one and only late night show with Jānis Skutelis.",
        blocks:[
            {type: 'vimeo', col: 12, videoId: '357569312', coverVideo: '/assets/img/cases/lzp/skiratlons-standup-case.webm' },
            // {type: 'image', col: 12, data: '/assets/img/cases/lzp/1.png' },
        ],
    },

    'insiders-guide-to-riga':{
        slug:'insiders-guide-to-riga',
        cover:'/assets/img/cases/insider.jpg',

        title:"Insider’s guide to Riga",
        client:'LIVE RIGA',
        cat: 'identity',
        excerpt: 'To promote Riga as a travel destination we created cinematic insider stories – from fact distilled into fiction and presented by tongue-in-cheek characters to real people and events with a surreal twist.',
        description:"Breaking through the noise of travel ads is tricky. And we love a good challenge. To put Riga on the map our approach was to embrace the clichés about our city instead of tiptoeing around them. We compressed them into a series of short films featuring fictional characters and a real celebrity. By the way, this may very well be our most awarded campaign of all time. For now.",
        blocks:[
            {type: 'vimeo', col: 12, videoId: '363523207', coverVideo: '/assets/img/cases/insider/liveriga_runners_guide_preview.webm' },
            {type: 'vimeo', col: 12, videoId: '297289175', coverVideo: '/assets/img/cases/insider/liveriga_policeman_preview.webm' },
            {type: 'vimeo', col: 12, videoId: '283647165', coverVideo: '/assets/img/cases/insider/liveriga_tennis_guide_preview.webm' },
            {type: 'vimeo', col: 12, videoId: '239956262', coverVideo: '/assets/img/cases/insider/liveriga_godson_preview.webm' },
            {type: 'vimeo', col: 12, videoId: '239956075', coverVideo: '/assets/img/cases/insider/liveriga_birdwatcher_preview.webm' },
            {type: 'vimeo', col: 12, videoId: '216648580', coverVideo: '/assets/img/cases/insider/liveriga_taxi_driver_preview.webm' },
            {type: 'vimeo', col: 12, videoId: '216648107', coverVideo: '/assets/img/cases/insider/liveriga_senior_preview_2.webm' },
            {type: 'vimeo', col: 12, videoId: '216648047', coverVideo: '/assets/img/cases/insider/liveriga_priest_preview.webm' },
            {type: 'vimeo', col: 12, videoId: '216647570', coverVideo: '/assets/img/cases/insider/liveriga_blogger_preview.webm' },
        ],
    },
}

export const get = (req, res) => {
    function takevalues(item) {
        return {
            slug: item.slug,
            cover: item.cover,
            title: item.title,
            client: item.client,
        };
    }

    function next(item) {
        let keys = Object.keys(cases);
        let index = keys.indexOf(item) + 1;

        if (index >= keys.length) {
            index = 0;
        }

        return takevalues(cases[keys[index]]);
    }

    function prev(item) {
        let keys = Object.keys(cases);
        let index = keys.indexOf(item) - 1;

        if (index < 0) {
            index = keys.length - 1;
        }

        return takevalues(cases[keys[index]])
    }

    const { slug } = req.params
    let returnValue = null;

    if (slug == 'all') {
        returnValue = cases;
    } else {
        returnValue = cases[slug];

        try {
            returnValue.next = next(slug);
        } catch (e) {}

        try {
            returnValue.prev = prev(slug);
        } catch (e) {}
    }

    res.writeHead(200, { 'Content-Type': 'application/json' })
    res.end( JSON.stringify( returnValue ) )
}
